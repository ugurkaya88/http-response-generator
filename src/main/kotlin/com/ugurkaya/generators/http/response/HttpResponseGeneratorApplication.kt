package com.ugurkaya.generators.http.response

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class HttpResponseGeneratorApplication

fun main(args: Array<String>) {
	runApplication<HttpResponseGeneratorApplication>(*args)
}
