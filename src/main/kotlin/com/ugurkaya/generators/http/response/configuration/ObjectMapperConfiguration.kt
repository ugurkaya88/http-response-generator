package com.ugurkaya.generators.http.response.configuration

import com.fasterxml.jackson.annotation.JsonInclude
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder

@Configuration
class ObjectMapperConfiguration {
    @Bean
    fun objectMapperBuilder(): Jackson2ObjectMapperBuilder? {
        return Jackson2ObjectMapperBuilder().apply {
            serializationInclusion(JsonInclude.Include.NON_NULL)
        }
    }
}
