package com.ugurkaya.generators.http.response.interceptor

import com.ugurkaya.generators.http.response.stereotype.ErrorResponse
import com.ugurkaya.generators.http.response.stereotype.exception.IllegalStatusCodeException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(value = [IllegalStatusCodeException::class])
    fun handleIllegalArgumentException(ex: IllegalStatusCodeException, request: WebRequest): ResponseEntity<Any> {
        return ResponseEntity(ErrorResponse(ex.code, ex.description, ex.hint), HttpStatus.BAD_REQUEST)
    }
}