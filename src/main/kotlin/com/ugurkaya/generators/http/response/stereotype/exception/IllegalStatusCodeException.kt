package com.ugurkaya.generators.http.response.stereotype.exception

data class IllegalStatusCodeException (val code: String, val description: String, val hint: String? = null)
    : Exception ("$code: $description")