package com.ugurkaya.generators.http.response.stereotype

data class ErrorResponse(val code: String, val description: String, val hint: String? = null)