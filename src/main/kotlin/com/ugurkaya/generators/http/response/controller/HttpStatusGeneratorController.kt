package com.ugurkaya.generators.http.response.controller

import com.ugurkaya.generators.http.response.stereotype.exception.IllegalStatusCodeException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder

@RestController
@RequestMapping("generate")
class HttpStatusGeneratorController {
    @RequestMapping("/{statusCode}")
    fun getResponse(@PathVariable statusCode: String) : ResponseEntity<Any> {
        val statusCodeValue = statusCode.toIntOrNull() ?: throw IllegalStatusCodeException("ERR-001",
                                                                "Provided status code is malformed. Please provide a valid status code.",
                                                                this.hint)
    
        return ResponseEntity(HttpStatus.resolve(statusCodeValue) ?: throw IllegalStatusCodeException("ERR-002",
                                                                "HttpStatus: $statusCode does not exist.",
                                                                this.hint))
    }
    
    private val hint: String
        get() = "Sample usage: ${ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString()}/200"
}
