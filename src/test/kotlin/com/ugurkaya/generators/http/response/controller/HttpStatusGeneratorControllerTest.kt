package com.ugurkaya.generators.http.response.controller

import com.ugurkaya.generators.http.response.stereotype.exception.IllegalStatusCodeException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes

internal class HttpStatusGeneratorControllerTest {
    private val controller = HttpStatusGeneratorController()
    
    @BeforeEach
    fun setUp() {
        RequestContextHolder.setRequestAttributes(ServletRequestAttributes(MockHttpServletRequest()))
    }
    
    @Test
    fun `Successful responses should be generated`() {
        HttpStatus.values().forEach {
            assertEquals(it.value(), controller.getResponse(it.value().toString()).statusCode.value())
        }
    }
    
    @Test
    fun `Failed responses should throw IllegalStatusCodeException`() {
        assertThrows(IllegalStatusCodeException::class.java) { controller.getResponse(12345.toString()) }
        assertThrows(IllegalStatusCodeException::class.java) { controller.getResponse(0.toString()) }
        assertThrows(IllegalStatusCodeException::class.java) { controller.getResponse(40.3.toString()) }
        assertThrows(IllegalStatusCodeException::class.java) { controller.getResponse("InvalidCode") }
        assertThrows(IllegalStatusCodeException::class.java) { controller.getResponse("") }
    }
}