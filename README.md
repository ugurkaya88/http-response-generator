# The Challenge

While integrating another REST API into your project, it takes much more time than intended to simulate different responses from the API as the request body and/or the parameters are required to be prepared for each status code. Even worse, it might be very though to simulate network related response codes like `504 - Gateway Timeout` or server errors like `500 - Internal Server Error`.

Using this service, simply simulating any HTTP response is very easy.

### How to use

Make a request in the following format: `/generate/{httpResponseCode}`
You can use any request method like `GET`, `POST`, `DELETE`, etc.

```http request
GET /generate/202 HTTP/1.1

HTTP/1.1 202 Accepted
```

```http request
GET /generate/404 HTTP/1.1

HTTP/1.1 404 Not Found
```

```http request
GET /generate/504 HTTP/1.1

HTTP/1.1 504 Gateway Timeout
```

### Invalid Request Handling

If the requested HTTP status code is invalid, the service returns a default response (400 Bad Request) with the usage:

```http request
GET /generate/error HTTP/1.1

HTTP/1.1 400 Bad Request
{
    "code": "ERR-001",
    "description": "Provided status code is malformed. Please provide a valid status code.",
    "hint": "Sample usage: http://localhost:8881/200"
}
```

```http request
GET /generate/123 HTTP/1.1

HTTP/1.1 400 Bad Request
{
    "code": "ERR-002",
    "description": "HttpStatus: 123 does not exist.",
    "hint": "Sample usage: http://0.0.0.0:8881/200"
}
```

### Future Improvements

Currently, only the HTTP status code is returned in the response. However, to be more realistic, it could be handy to return the body with some content as well for certain HTTP Status Codes.
